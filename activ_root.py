import numpy as np
import ROOT as r

# Define the activation functions
def relu(x):
    return np.maximum(0, x)

def tanh(x):
    return np.tanh(x)

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# Create canvas and graph objects
canvas = r.TCanvas("canvas", "Activation Functions", 800, 600)
legend = r.TLegend(0.79, 0.15, 0.99, 0.3) # set legend position
graph_relu = r.TGraph()
graph_tanh = r.TGraph()
graph_sigmoid = r.TGraph()

# Set graph styles
graph_relu.SetLineColor(r.kBlue)
graph_relu.SetLineWidth(2)
graph_tanh.SetLineColor(r.kGreen+2)
graph_tanh.SetLineWidth(2)
graph_tanh.SetLineStyle(2)
graph_sigmoid.SetLineColor(r.kRed)
graph_sigmoid.SetLineWidth(2)
graph_sigmoid.SetLineStyle(2)
graph_sigmoid.GetXaxis().SetTitleSize(0.05)# Fill graphs with data points
graph_sigmoid.GetYaxis().SetTitleSize(0.05)# Fill graphs with data points
graph_tanh.GetXaxis().SetTitleSize(0.05)# Fill graphs with data points
graph_tanh.GetYaxis().SetTitleSize(0.05)# Fill graphs with data points
graph_relu.GetXaxis().SetTitleSize(0.05)# Fill graphs with data points
graph_relu.GetYaxis().SetTitleSize(0.05)# Fill graphs with data points
x_vals = np.linspace(-3, 3, 1000)
y_vals_relu = [relu(x) for x in x_vals]
y_vals_tanh = [tanh(x) for x in x_vals]
y_vals_sigmoid = [sigmoid(x) for x in x_vals]

for i in range(len(x_vals)):
    graph_relu.SetPoint(i, x_vals[i], y_vals_relu[i])
    graph_tanh.SetPoint(i, x_vals[i], y_vals_tanh[i])
    graph_sigmoid.SetPoint(i, x_vals[i], y_vals_sigmoid[i])

# Set axis titles and ranges
graph_relu.GetXaxis().SetTitle("Input")
graph_relu.GetYaxis().SetTitle("Output")
graph_relu.GetXaxis().SetRangeUser(-3, 3)
graph_relu.GetYaxis().SetRangeUser(-1, 3)
graph_relu.GetXaxis().SetLabelSize(0.05)
graph_relu.GetYaxis().SetLabelSize(0.05)

graph_relu.GetXaxis().SetTitleSize(0.05)
graph_relu.GetYaxis().SetTitleSize(0.05)
# Add graphs and legend entries
graph_relu.Draw("AL")
legend.AddEntry(graph_relu, "ReLU", "l")
graph_tanh.Draw("L SAME")
legend.AddEntry(graph_tanh, "tanh", "l")
graph_sigmoid.Draw("L SAME")
legend.AddEntry(graph_sigmoid, "sigmoid", "l")

# Add legend to canvas
legend.Draw()

canvas.SetBottomMargin(0.15)
canvas.SetRightMargin(0.01)
canvas.SetLeftMargin(0.15)

# Save canvas as image file
canvas.SaveAs("activation_functions.png")
