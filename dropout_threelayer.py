import numpy as np
import os
import math
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv
import torchvision.transforms as tfms
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD
from torch.utils.data import Dataset
from sklearn.inspection import permutation_importance
from skorch import NeuralNetClassifier

class Dataset_tr(Dataset):
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        label = self.labels[idx]
        return meas, label
    
class Dataset_test(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        return meas

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.first = nn.Sequential(
            nn.Dropout(0.2), 
            nn.Linear(6, 16),
            nn.BatchNorm1d(16),
            nn.ReLU())

        self.second = nn.Sequential(
            nn.Dropout(0.2), 
            nn.Linear(16, 4),
            nn.BatchNorm1d(4),
            nn.ReLU())

        self.third = nn.Sequential(
            nn.Dropout(0.2), 
            nn.Linear(4, 2),
            nn.Softmax(dim=1))
	
        self.weight_init()

    def weight_init(self):
        for lay in self.modules():
            if type(lay) == torch.nn.Linear:
                torch.nn.init.xavier_uniform_(lay.weight)

    def forward(self, x):
        x = self.first(x)
        x = self.second(x)
        x = self.third(x)
        return x

def load_bar(percent):
    resolution = 30 #number of equal signs
    string = '\r'
    #print(percent)
    for i in range(int(resolution*percent/100)):
        string = string + '='
    string = string + '>'
    print(string)
    return 0

def train_epoch(model, train_loader, loss_fn, optimizer, device, num_of_samples):
    model.train()
    loss_sum = 0
    for idx, batch in enumerate(train_loader):
        #if idx%1000 == 0:
             #print(num_of_samples, idx)
             #print(64*idx/num_of_samples)
        #     load_bar(int(64*idx/num_of_samples))
        data, labels = batch
        labels = labels.reshape(-1)
        data = data.to(device)
        labels = labels.long()
        labels = labels.to(device)
        output = model(data.float())
        loss = loss_fn(output, labels)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        loss_sum += loss.item()
    loss = loss_sum / (idx+1)
    return loss


def validate_model(model, val_loader, device):
    tn, tp, fn, fp = 0, 0, 0, 0
    model.eval()
    with torch.no_grad():
        loss_sum = 0
        for idx, batch in enumerate(val_loader):
            data, labels = batch		#labels is actually only one label
            labels = labels.reshape(-1)
            data = data.to(device)
            labels = labels.long()
            labels = labels.to(device)
            output = model(data.float())
            loss = loss_fn(output, labels)
            loss_sum += loss.item()
            labels = int(labels) #threshold value - now its 0.5
            #print(output) tensor([[1,0]])
            #output = np.argmax(output.tolist())
            # zalezi jestli je output cislo nebo arr idk debug
            thr = 0.03 
            #print(output, labels)
            output = float(output[0][1])
            if output > thr:
                output = 1
            else:
                output = 0

            if output == 0 and labels == 0:
                tn += 1
            elif output == 0 and labels == 1:
                fn += 1
            elif output == 1 and labels == 1:
                tp += 1
            elif output == 1 and labels == 0:
                fp += 1
            else:
                print("wrong format - validate model not working")
    # validation results
    print(f"TN: {tn}\nTP: {tp}\nFN: {fn}\nFP: {fp}\n")
    data = {
        "Predicted Signal":  [tp / (tp+fn), fp / (tn+fp)],
        "Predicted Background": [fn / (tp+fn), tn / (tn+fp)]
    }
    #df = pd.DataFrame(data, index=["Real Signal", "Real Background"])
    #conf = sns.heatmap(df, annot=True)
    #plt.show()
    loss = loss_sum / (idx+1)
    return loss


def get_device():
    if torch.cuda.is_available():
        gpu = get_free_gpu()
        print("Found GPU\n")
        device = torch.device(gpu)
    else:
        device = 'cpu'
    return device


def load_model():
    model = Model().to(device)
    model.load_state_dict(torch.load(
        f'dweights.pth', map_location=torch.device('cpu')))
    return model


def csv2arr(filename,labels=False):
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    if labels:
        ctr_s = 0
        ctr_b = 0
        for row in csvreader:
            if int(row[0]) == 0:
                 ctr_b += 1
            elif int(row[0]) == 1:
                 ctr_s += 1
            else:
                 print(row)
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
        file.close()
        return np.array(rows), header, ctr_s, ctr_b
    else:
        for row in csvreader:
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
    file.close()
    return np.array(rows), header

def get_imp(model, data, labels):
    model = NeuralNetClassifier(
        model,
        criterion=nn.CrossEntropyLoss,
    )
    model.initialize()
    model.load_params(f_params='dweights.pth')
    imp = permutation_importance(model, data.astype('float32'), labels, n_repeats=10).importances_mean #default repeats=5                
    return imp/sum(imp)

def transform(data):
    mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
    std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]
    return (data-mean)/std

if __name__ == '__main__':

    train_nn = False # If this is False, you can test the network on a file with old weights

    # TODO add transforms - normalize,
    BATCH_SIZE = 64
    EPOCHS = 15
    NUM_WORKERS = 10
    datapr = 0.8  # TR to VAL proportions     All data are divided into 0.7 and 0.3 for testing and 0.7 is now divided into TR and VAL
    if train_nn:

        # data,labels = get_data_labels(signal_file,background_file)
        data, _ = csv2arr('train_data.csv')
        labels, _, ctr_s, ctr_b = csv2arr('train_labels.csv',True)
        train_thresh = int(datapr*len(data))
        data = transform(data)
        # Splitting the data
        tr_data = torch.from_numpy(data[0:train_thresh])
        tr_labels = torch.from_numpy(labels[0:train_thresh]) 
        val_data = torch.from_numpy(data[train_thresh:len(data)])
        val_labels = torch.from_numpy(labels[train_thresh:len(data)]) 
        #calculating std and mean
        mean = np.mean(data,axis=0)
        std = np.std(data,axis=0)
        #transform = tfms.Compose([tfms.Normalize(mean,std)])
        train_dataset = Dataset_tr(tr_data,tr_labels) 
        val_dataset = Dataset_tr(val_data,val_labels)
        train_dataloader = DataLoader(
            dataset=train_dataset, batch_size=BATCH_SIZE, num_workers=NUM_WORKERS, shuffle=True)
        val_dataloader = DataLoader(
            dataset=val_dataset, batch_size=1, shuffle=True)
        device = get_device()
        model = Model().to(device)
        occurance = torch.tensor([ctr_b, ctr_s])
        weight = 1/occurance
        weight = weight/sum(weight)
        num_of_samples = ctr_b+ctr_s
        loss_fn = nn.CrossEntropyLoss(weight=weight).to(device)
        opt = Adam(model.parameters(), lr=0.001, betas=(
            0.9, 0.999), eps=1e-08, weight_decay=0)
        tr_losstmp = float('inf')
        val_losstmp = float('inf')
        graph_buffer_val = []
        graph_buffer_tr  = []
        for i in range(EPOCHS):
            print(f"Start of Epoch {i}")
            tr_loss = train_epoch(model, train_loader=train_dataloader,
                                  loss_fn=loss_fn, optimizer=opt, device=device,num_of_samples=num_of_samples)
            print(f"Train loss is: {tr_loss}, starting validation")
            val_loss = validate_model(
                model, val_loader=val_dataloader, device=device)
            print(f"Validation loss is: {val_loss}")

            if (val_loss < val_losstmp) and (tr_loss < tr_losstmp):
                torch.save(model.state_dict(), 'dweights.pth')
            tr_losstmp = tr_loss
            val_losstmp = val_loss
            graph_buffer_tr.append(tr_loss)
            graph_buffer_val.append(val_loss)
        
        plt.cla()
        plt.clf()
        ep = np.arange(EPOCHS)
        plt.plot(ep, graph_buffer_tr, label="Training loss")
        plt.plot(ep, graph_buffer_val, label="Validation loss")
        plt.xlabel("Epochs")
        plt.ylabel("Loss")
        plt.legend(title="Legend")
        plt.savefig("loss.png")
        plt.show()
        
    else:
        mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
        std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]

        #transform = tfms.Compose([tfms.Normalize(mean,std)])
        data, _ = csv2arr('test_data.csv')
        labels, _, _, _ = csv2arr('test_labels.csv',True)
        data = transform(data) 
        data = torch.from_numpy(data)
        val_dataset = Dataset_tr(data,torch.from_numpy(labels))
        val_dataloader = DataLoader(dataset=val_dataset, batch_size=1, shuffle=True)
        device = get_device()
        model = load_model()
        loss_fn = nn.CrossEntropyLoss().to(device)
        
        #imp = get_imp(model, data, labels)
        #print(f'Feature importance is: {imp}')
        
        val_loss = validate_model(model, val_loader=val_dataloader, device=device)
        print(f"Test loss is: {val_loss}")
        features = ["PhotonN","Aco","Eta 0", "Eta 1", "Pt 0", "Pt 1"]
        #fig = plt.figure()
        #plt.bar(features, imp)
        #plt.ylabel("Feature importance")
        #plt.xlabel("Features")
        #plt.savefig("feat_imp.png")
        #plt.show()
