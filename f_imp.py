import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import numpy as np
import csv
from  sklearn.inspection import permutation_importance
from skorch import NeuralNetClassifier
import shap
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import ROOT as root
import copy
class Dataset_tr(Dataset):
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        label = self.labels[idx]
        return meas, label
    

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.first = nn.Sequential(
            nn.Linear(6, 16),
            nn.BatchNorm1d(16),
            nn.ReLU())

        self.second = nn.Sequential(
            nn.Linear(16, 4),
            nn.BatchNorm1d(4),
            nn.ReLU())

        self.third = nn.Sequential(
            nn.Linear(4, 2),
            nn.Softmax(dim=1))
	
        self.weight_init()

    def weight_init(self):
        for lay in self.modules():
            if type(lay) == torch.nn.Linear:
                torch.nn.init.xavier_uniform_(lay.weight)

    def forward(self, x):
        x = self.first(x)
        x = self.second(x)
        x = self.third(x)
        return x

def load_model():
    model = Model().to(device)
    model.load_state_dict(torch.load(
        f'weights.pth', map_location=torch.device('cpu')))
    return model

def get_device():
    if torch.cuda.is_available():
        gpu = get_free_gpu()
        print("Found GPU\n")
        device = torch.device(gpu)
    else:
        device = 'cpu'
    return device

def get_imp(model, data, labels):
    model = NeuralNetClassifier(
        model,
        criterion=nn.CrossEntropyLoss,
    )
    model.initialize()
    model.load_params(f_params='weights.pth')
    return permutation_importance(model, data.astype('float32'), labels, n_repeats=10).importances_mean #default repeats=5                

def csv2arr(filename,labels=False):
    #Labels=True says, that the file contains labels
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    if labels:
        ctr_s = 0
        ctr_b = 0
        for row in csvreader:
            if int(row[0]) == 0:
                 ctr_b += 1
            elif int(row[0]) == 1:
                 ctr_s += 1
            else:
                 print(row)
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
        file.close()
        return np.array(rows), header, ctr_s, ctr_b
    else:
        for row in csvreader:
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
    file.close()
    return np.array(rows), header

def transform(data):
    mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
    std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]
    return (data-mean)/std

if __name__ == "__main__":
    print("Starting\n")
    device = get_device()
    model = load_model()
    bg, _ = csv2arr("recorded_data.csv")
    sig, _ = csv2arr("alp_sd_m300.csv")
    #labels,_ , _, _ = csv2arr("test_labels.csv",labels=True)
    bg = transform(bg)
    sig = transform(sig)
    #print(data.shape)
    #indices_s = np.argwhere(labels==1)[:,0]
    #indices_b = np.argwhere(labels==0)[:,0]
    #imp = get_imp(model, data, labels)
    #print(imp)
    #val_dataset = Dataset_tr(torch.from_numpy(data),torch.from_numpy(labels))#Dataset_tr(torch.from_numpy(data[train_thresh:len(data)]),torch.from_numpy(labels[train_thresh:len(data)]))
    #val_dataloader = DataLoader(
    #    dataset=val_dataset, batch_size=1, shuffle=True)
    #x, y = next(iter(val_dataloader))
    #sig = data[indices_s]
    #bg = data[indices_b]
    #sig = sig[2000:20002] #100 :(
    #bg = bg[2000:2002] #100
    # print(explainer.expected_value)
    # shap_values = np.delete(shap_values, 0, axis=1)
  
    #shap_values = shap_values[:,:,1:6]
    #shap_mean = np.mean(shap_values, axis=0)
    #shap_std = np.std(shap_values, axis=0)
    #print(shap_values)
    feature_names = ['Acoplanarity','Leading photon pseudorapidity','Subleading photon pseudorapidity','Leading photon transverse momentum','Subleading photon transverse momentum']  # list of feature names
    #fig = shap.summary_plot(shap_values, plot_type="bar",plot_std=True,feature_names=feature_names, show=False)
    summ = []
    sums = []
    sample_size = 1000
    for i in range(50):    
        print(i)
        indices = np.random.choice(sig.shape[0], size=sample_size, replace=False)
        s = copy.deepcopy(sig[indices])
        indices = np.random.choice(bg.shape[0], size=sample_size, replace=False)
        b = copy.deepcopy(bg[indices])
    
        ##print(sig.shape)
        s = torch.from_numpy(s.astype(np.float32))
        b = torch.from_numpy(b.astype(np.float32))
        explainer = shap.DeepExplainer(model,b)
        shap_values = explainer.shap_values(s)
        shaps = shap_values[1]
        shaps = np.abs(shaps[:,1:6])
        mean = (np.mean(shaps,axis=0))
        std = np.std(shaps,axis=0)
        summ.append(mean)
        #print(sums)
        sums.append(std)
    #print(np.mean(summ,axis=0))
    #print(np.std(summ,axis=0))
    #print(std) 
    #shap.plots.beeswarm(shap_values=shap_values, color='Class')
    #bee_swarm = shap.plots.beeswarm(shap.Explanation(
    #values=shap_values, 
    #data=s,
    #feature_names=feature_names
    #), c#olor='Class')
    #                # plt.xticks(range(shap_values.shape[1]), feature_names)
    #shap.plots.beeswarm(shap_values)#, feature_names=feature_names, color='Class')
    #print(shap)
    #print(shap)
    #print(mean)
    #print(std)
    #shap_std = shap_values[0].std(axis=0)[1:6]
    # shap_mean = shap_values[0].mean(axis=0)[1:6]
    mean = np.mean(summ,axis=0)
    std = np.std(summ,axis=0)
    indices = np.argsort(mean)
    mean = mean[indices]
    std = std[indices]
    #print(indices)
   
    feature_names = np.take(feature_names, indices, axis=0)
    plt.barh(feature_names, mean,xerr=std, alpha=0.8, edgecolor='black')
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)

    #n, bins, patches = plt.hist(means, bins=30,histtype='stepfilled', edgecolor='black')
    #plt.errorbar(feature_names, means, yerr=std, fmt='none', ecolor='red', capsize=4)
    plt.xlabel('SHAP values',fontsize=17)
    #plt.ylabel()
    # plt.title('Plot with Error Bars')



    #ax.set_ylabel("SHAP value")
    #ax.set_title("Feature importance")
    plt.savefig("feat_imp_sd_m300.png",bbox_inches='tight')
    plt.show()
