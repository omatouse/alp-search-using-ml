import ROOT as root
import atlasplots as aplt
import math, sys, os
import Base
from scipy import integrate

def binWidth(hist):
    return hist.GetXaxis().GetBinWidth(1)

def binWidthX(hist):
    return binWidth(hist)

def binWidthXStr(hist):
    return str(binWidthX(hist))

def binWidthY(hist):
    return hist.GetYaxis().GetBinWidth(1)

def binWidthYStr(hist):
    return str(binWidthY(hist))

def binWidthStr(hist):
    return str(binWidth(hist))

def align(ax, bottom_margin=False, top_margin=True, left_margin=False, right_margin=False):
    ax.add_margins(top=0.125)
    if bottom_margin:
        ax.add_margins(bottom=0.125)
    if left_margin:
        ax.add_margins(left=0.05)
    if right_margin:
        ax.add_margins(right=0.05)
    ax.cd()
    aplt.atlas_label(text="Simulation Internal", loc="upper left")
    ax.text(0.2, 0.84, "#sqrt{s} = 13 TeV, 14.6 fb^{-1}", size=22, align=13)

def getYLabel(hist = root.TH1D(), unit = ""):
    if unit == "":
        return "Events / " + binWidthStr(hist)
    else:
        return "Events / " + binWidthStr(hist) + " [" + unit + "]"

def getZLabel(hist = root.TH2D(), xunit = "", yunit = ""):
    return "Events / " + binWidthXStr(hist) + ("" if xunit=="" else (" [" + xunit + "]")) + " / " + binWidthYStr(hist) + ("" if yunit=="" else (" [" + yunit + "]"))

def plotSingleHist(hist = root.TH1D(), xlabel = "", ylabel = "", canvname = "", xlogscale = False, ylogscale = False, errorband = False, ylim = [], objects = [], pdfname="", text=("", 0, 0)):
    fig, ax = aplt.subplots(1, 1)
    ax.plot(hist, linecolor=root.kBlue)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlogscale:
        ax.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    if errorband:
        err_band = aplt.root_helpers.hist_to_graph(hist, show_bin_width=True)
        ax.plot(err_band, "2", fillcolor=1, fillstyle=3254, linewidth=0)
    if ylim:
        ax.set_ylim(ylim[0], ylim[1])
    align(ax, True)
    if text[0]:
        ax.text(text[1], text[2], text[0], size=22, align=13)
    if canvname == "":
        fig.canvas.SetName(hist.GetName())
    else:
        fig.canvas.SetName(canvname)
    fig.canvas.Write()
    if pdfname:
        fig.canvas.Print(pdfname)

colorList = [root.kOrange+10, root.kAzure-6, root.kGreen+2, root.kOrange+1, root.kAzure+8, root.kRed-9, root.kRed-2, root.kMagenta-2]
fillColorList = [root.kAzure-9, root.kYellow-9, root.kTeal-8, root.kOrange-4, root.kBlue-10, root.kRed-10, root.kYellow+1]

def plotMultiHist(hist = [], label = [], xlabel = "", ylabel = "", canvname = "", xlogscale = False, ylogscale = False, normalize = False, stack = False, ratio = False, options = "", sort = False, pdfname="", text=("", 0, 0)):
    if (len(hist) != len(label) or len(hist) == 0):
        print("ERROR: Length of input hist or label of plotMultiHist is wrong.")
        sys.exit(1)
    if sort:
        ltmp = [[h, l] for h, l in zip(hist, label)]
        ltmp = sorted(ltmp, reverse=False, key=lambda pair : pair[0].Integral(1, pair[0].GetNbinsX()))
        hist = [pair[0] for pair in ltmp]
        label = [pair[1] for pair in ltmp]
    if ratio:
        fig, (ax, ax2) = aplt.ratio_plot(name=canvname, figsize=(800, 600), hspace=0)
    else:
        fig, ax = aplt.subplots(1, 1)
    if stack:
        for i in range(len(hist)):
            hist[i].SetFillColor(Base.fillColorList[i % len(Base.fillColorList)])
            hist[i].SetLineStyle(int(i/len(Base.fillColorList)+1))
            hist[i].Sumw2()
            hs_mc = root.THStack("canvname", "")
            for i in range(len(hist)):
                hs_mc.Add(hist[i])
            if options == "":
                ax.plot(hs_mc)
            else:
                ax.plot(hs_mc, options=options)
            legend = root.TLegend(0.65, 0.8, 0.95, 0.92)
            legend.SetFillStyle(0)
            legend.SetTextSize(22)
            for i in range(len(hist)):
                legend.AddEntry(hist[i], label[i], "F")
            legend.Draw()
    else:
        if normalize:
            area = lambda h : h.Integral(1, h.GetNbinsX())
            h0Area = area(hist[0])
            if h0Area:
                for h in hist[1:]:
                    h.Sumw2()
                    if area(h):
                        h.Scale(h0Area / area(h))
                #for i in range(1, len(hist)):
                #    hist[i].Sumw2()
                #    hist[i].Scale(h0Area / area(hist[i]))
        for i in range(len(hist)):
            if options == "":
                ax.plot(hist[i], linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1), label=label[i], labelfmt="f")
            else:
                ax.plot(hist[i], options=options, linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1), label=label[i], labelfmt="f")
    if ratio:
        h_ratio = [h.Clone() for h in hist]
        for i in range(len(h_ratio)):
            h_ratio[i].Divide(hist[0])
            if options == "":
                ax2.plot(h_ratio[i], linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1))
            else:
                ax2.plot(h_ratio[i], options=options, linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1))
    if ratio:
        ax2.set_xlabel(xlabel)
    else:
        ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlogscale:
        ax.set_xscale("log")
        if ratio:
            ax2.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    align(ax)
    if text[0]:
        ax.text(text[1], text[2], text[0], size=22, align=13)
    if not stack:
        ax.legend(loc=(0.65, 0.8, 0.95, 0.92), fillstyle=0)
    if canvname == "":
        fig.canvas.SetName(hist[0].GetName())
    else:
        fig.canvas.SetName(canvname)
    fig.canvas.Write()
    if pdfname:
        fig.canvas.Print(pdfname)

def plot2DHist(hist = root.TH2D(), xlabel = "", ylabel = "", zlabel = "", canvname = "", xlogscale=False, ylogscale=False, zlogscale=False, figsize=(600, 600), pdfname="", text=("", 0, 0)):
    fig, ax = aplt.subplots(1, 1, name=(hist.GetName() if canvname == "" else canvname), figsize=figsize)
    ax.plot2d(hist, options="colz")
    ax.set_pad_margins(right=0.20, top=0.08)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel, titleoffset=1.2)
    if xlogscale:
        ax.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    if zlogscale:
        ax.set_zscale("log")
    aplt.atlas_label(ax.pad.GetLeftMargin(), 0.97, text="Simulation Internal", align=13)
    if text[0]:
        ax.text(text[1], text[2], text[0], size=22, align=13)
    fig.canvas.Write()
    if pdfname:
        fig.canvas.Print(pdfname)

def plotDataMC(hist = [], label = [], xlabel = "", ylabel = "", canvname = "", ylogscale = False, ylim = [0.1, 1.9]):
    fig, (ax1, ax2) = aplt.ratio_plot(name=canvname, figsize=(800, 600), hspace=0)
    for i in range(1, len(hist)):
        hist[i].SetFillColor(Base.fillColorList[(i-1) % len(Base.fillColorList)])
        hist[i].Sumw2()
    hist[0].Sumw2()
    hist[0].SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)  # Use 68% Poisson errors
    hs_mc = root.THStack("mc", "")
    for i in range(1, len(hist)):
        hs_mc.Add(hist[i])
    ax1.plot(hs_mc)

    err_band = aplt.root_helpers.hist_to_graph(hs_mc.GetStack().Last(), show_bin_width=True)
    ax1.plot(err_band, "2", fillcolor=root.kRed, fillstyle=3245)

    err_band_stat = aplt.root_helpers.hist_to_graph(hs_mc.GetStack().Last(), show_bin_width=True, bin_err="W-")
    ax1.plot(err_band_stat, "2", fillcolor=1, fillstyle=3254)

    if ylogscale:
        ax1.set_yscale("log")

    data_graph = aplt.root_helpers.hist_to_graph(hist[0])
    ax1.plot(data_graph, "PZ0")

    ax2.set_xlim(ax1.get_xlim())
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    err_band_ratio = aplt.root_helpers.hist_to_graph(hs_mc.GetStack().Last(), show_bin_width=True, norm=True)
    ax2.plot(err_band_ratio, "2", fillcolor=root.kRed, fillstyle=3245)

    err_band_stat_ratio = aplt.root_helpers.hist_to_graph(hs_mc.GetStack().Last(), show_bin_width=True, norm=True, bin_err="W-")
    ax2.plot(err_band_stat_ratio, "2", fillcolor=1, fillstyle=3254)

    ratio_hist = hist[0].Clone("ratio_hist")
    ratio_hist.Divide(hs_mc.GetStack().Last())
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ax2.plot(ratio_graph, "PZ0")

    ax1.add_margins(top=0.16)
    ax2.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax2.set_ylabel("Data / Pred.", loc="centre")
    ax2.set_ylim(ylim[0], ylim[1])

    ax1.cd()
    aplt.atlas_label(text="Internal", loc="upper left")
    ax1.text(0.2, 0.84, "#sqrt{s} = 13 TeV, 14.6 fb^{-1}", size=22, align=13)
    legend = root.TLegend(0.65, 0.8, 0.95, 0.92)
    legend.SetFillStyle(0)
    legend.SetTextSize(22)
    legend.AddEntry(data_graph, label[0], "EP")
    legend.AddEntry(err_band, "BG Shape Unc.", "F")
    legend.AddEntry(err_band_stat, "BG Stat. Unc.", "F")
    for i in range(1, len(hist)):
        legend.AddEntry(hist[i], label[i], "F")
    legend.Draw()

    fig.canvas.Write()

def satisfyMatchingCondition(mcondition, a, c):
    l = [a and not c, not a and c, a and c, not (a or c)]
    for i in range(4):
        if (mcondition[i] and l[i]):
            return True
    return False

def satisfyRegion(val, region):
    lower = True
    good = False
    for x in region:
        if lower:
            if x <= val:
                good = True
            else:
                return False
            lower = False
        elif good:
            if val < x:
                return True
            else:
                good = False
            lower = True
    return False

def slimProton(DiphotonXiA, DiphotonXiC, ProtonN, ProtonXi, ProtonAC):
    DeltaXiA = 999.
    ia = -1
    for i in range(ProtonN):
        if ProtonAC[i]:
            diff = ProtonXi[i] - DiphotonXiA
            if abs(diff) < abs(DeltaXiA):
                DeltaXiA = diff
                ia = i
    PXiA = ProtonXi[ia] if ia >= 0 else -1.

    DeltaXiC = 999.
    ic = -1
    for i in range(ProtonN):
        if not ProtonAC[i]:
            diff = ProtonXi[i] - DiphotonXiC
            if abs(diff) < abs(DeltaXiC):
                DeltaXiC = diff
                ic = i
    PXiC = ProtonXi[ic] if ic >= 0 else -1.

    n = int(ia >= 0) + int(ic >= 0)

    return n, PXiA, PXiC, DeltaXiA, DeltaXiC

def plotSingleGraph(graph = root.TGraph(), options="p", xlabel = "", ylabel = "", canvname = "", xlogscale = False, ylogscale = False, xlim = [], ylim = [], figsize=(800, 600), markercolor=root.kBlue, text=("", 0, 0), add=[], top_margin=False, bottom_margin=False, left_margin=False, right_margin=False):
    fig, ax = aplt.subplots(1, 1, figsize=figsize)
    ax.plot(graph, options, markercolor=markercolor)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlogscale:
        ax.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    if add:
        for a in add:
            ax.plot(a)
        ax.plot(graph, options, markercolor=markercolor)
    align(ax, top_margin=top_margin, bottom_margin=bottom_margin, left_margin=left_margin, right_margin=right_margin)
    if text[0]:
        ax.text(text[1], text[2], text[0], size=22, align=13)
    if xlim:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim:
        ax.set_ylim(ylim[0], ylim[1])
    if canvname == "":
        fig.canvas.SetName(graph.GetName())
    else:
        fig.canvas.SetName(canvname)
    fig.canvas.Write()

def plotGraphFunc(graph = root.TGraph(), func=root.TF1(), func_graph=None, options="pz0", xlabel = "", ylabel = "", canvname = "", xlogscale = False, ylogscale = False, xlim = [], ylim = [], figsize=(800, 600), markercolor=root.kBlack, text=("", 0, 0), add=[], top_margin=False, bottom_margin=False, left_margin=False, right_margin=False, ratio=False, diff=False, chi2=True, pdfname=""):
    if ratio and diff:
        print("WARNING: You cannot use ratio and diff options at the same time. The diff option has been turned off.")
        diff = False
    if ratio or diff:
        fig, (ax, ax2) = aplt.ratio_plot(name=canvname, figsize=figsize, hspace=0)
    else:
        fig, ax = aplt.subplots(1, 1, figsize=figsize)
    ax.plot(graph, options, markercolor=markercolor)
    if func_graph:
        ax.plot(func_graph, options="l", linecolor=root.kBlue)
    else:
        ax.plot(func, linecolor=root.kBlue)
    if ratio or diff:
        g_ratio = root.TGraphAsymmErrors(graph)
        npoints = graph.GetN()
        for j in range(npoints):
            binwidth = graph.GetErrorXlow(j) + graph.GetErrorXhigh(j)
            xl = graph.GetPointX(j) - graph.GetErrorXlow(j)
            xh = graph.GetPointX(j) + graph.GetErrorXhigh(j)
            if ratio:
                factor = binwidth / integrate.quad(func, xl, xh)[0]
                g_ratio.SetPointY(j, graph.GetPointY(j) * factor)
                g_ratio.SetPointEYlow(j, graph.GetErrorYlow(j) * factor)
                g_ratio.SetPointEYhigh(j, graph.GetErrorYhigh(j) * factor)
            elif diff:
                integral = integrate.quad(func, xl, xh)[0]
                g_ratio.SetPointY(j, (graph.GetPointY(j) - integral) * binwidth)
                g_ratio.SetPointEYlow(j, graph.GetErrorYlow(j) * binwidth)
                g_ratio.SetPointEYhigh(j, graph.GetErrorYhigh(j) * binwidth)
        g_line = root.TGraph()
        xl, xh = ax.get_xlim()
        g_line.SetPoint(0, xl, 1)
        g_line.SetPoint(1, xh, 1)
        ax2.plot(g_line, options="l", linecolor=root.kBlue)
        ax2.plot(g_ratio, options=options, markercolor=markercolor)
    if ratio or diff:
        ax2.set_xlabel(xlabel)
    else:
        ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlogscale:
        ax.set_xscale("log")
        if ratio or diff:
            ax2.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    if add:
        for a in add:
            ax.plot(a)
        ax.plot(graph, options, markercolor=markercolor)
    align(ax, top_margin=top_margin, bottom_margin=bottom_margin, left_margin=left_margin, right_margin=right_margin)
    if chi2:
        red_chi2 = graph.Chisquare(func, "R") / (graph.GetN()-1)
        ax.text(0.6, 0.8, "#chi^{2}/NDF = " + f"{red_chi2:.3f}", size=22, align=13)
    if text[0]:
        ax.text(text[1], text[2], text[0], size=22, align=13)
    if xlim:
        ax.set_xlim(xlim[0], xlim[1])
        if ratio or diff:
            ax2.set_xlim(xlim[0], xlim[1])
    if ylim:
        ax.set_ylim(ylim[0], ylim[1])
    if canvname == "":
        fig.canvas.SetName(graph.GetName())
    else:
        fig.canvas.SetName(canvname)
    fig.canvas.Write()
    if pdfname:
        fig.canvas.Print(pdfname)

def plotMultiGraph(graph = [], label = [], xlabel = "", ylabel = "", canvname = "multigraph", xlogscale = False, ylogscale = False, options = "l", top_margin=True, bottom_margin=False, left_margin=False, right_margin=False, ratio=False, diff=False, linewidth=1):
    if (len(graph) != len(label) or len(graph) == 0):
        print("ERROR: Length of input graph or label of plotMultiGraph is wrong.")
        sys.exit(1)
    if ratio and diff:
        print("WARNING: You cannot use ratio and diff options at the same time. The diff option has been turned off.")
        diff = False
    if ratio or diff:
        fig, (ax, ax2) = aplt.ratio_plot(name=canvname, figsize=(800, 600), hspace=0)
    else:
        fig, ax = aplt.subplots(1, 1)
    for i in range(len(graph)):
        ax.plot(graph[i], options=options, linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1), linewidth=linewidth, markercolor=colorList[i % len(colorList)], label=label[i], labelfmt=options)
    
    if ratio or diff:
        g_ratio = [root.TGraph(g) for g in graph]
        npoints = graph[0].GetN()
        for i in range(len(g_ratio)):
            if graph[i].GetN() != npoints:
                print("WARNING: number of points is not the same as the nominal -> skip this ratio graph")
                continue
            if ratio:
                for j in range(npoints):
                    g_ratio[i].SetPointY(j, graph[i].GetPointY(j) / graph[0].GetPointY(j))
            elif diff:
                for j in range(npoints):
                    g_ratio[i].SetPointY(j, graph[i].GetPointY(j) - graph[0].GetPointY(j))                
            ax2.plot(g_ratio[i], options=options, linecolor=colorList[i % len(colorList)], linestyle=int(i/len(colorList)+1), linewidth=linewidth, markercolor=colorList[i % len(colorList)])
    if ratio or diff:
        ax2.set_xlabel(xlabel)
    else:
        ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlogscale:
        ax.set_xscale("log")
        if ratio or diff:
            ax2.set_xscale("log")
    if ylogscale:
        ax.set_yscale("log")
    align(ax, top_margin=top_margin, bottom_margin=bottom_margin, left_margin=left_margin, right_margin=right_margin)
    if ratio or diff:
      if left_margin:
          ax2.add_margins(left=0.05)
      if right_margin:
          ax2.add_margins(right=0.05)
    ax.legend(loc=(0.65, 0.8, 0.95, 0.92), fillstyle=0)
    fig.canvas.SetName(canvname)
    fig.canvas.Write()

def plotHistMC(hist = root.TH1D(), graph = root.TGraph(), label = [], xlabel = "", ylabel = "", canvname = "", ylogscale = False):
    fig, ax = aplt.subplots(1, 1)
    hist.Sumw2()
    hist.SetBinErrorOption(root.TH1.EBinErrorOpt.kPoisson)  # Use 68% Poisson errors
    for i in range(len(graph)):
        ax.plot(graph[i], linecolor=Base.colorList[(i) % len(Base.colorList)])

    if ylogscale:
        ax.set_yscale("log")

    data_graph = aplt.root_helpers.hist_to_graph(hist)
    ax.plot(data_graph, "PZ0")

    ax.add_margins(top=0.16)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    aplt.atlas_label(text="Internal", loc="upper left")
    ax.text(0.2, 0.84, "#sqrt{s} = 13 TeV, 14.6 fb^{-1}", size=22, align=13)
    legend = root.TLegend(0.65, 0.8, 0.95, 0.92)
    legend.SetFillStyle(0)
    legend.SetTextSize(22)
    legend.AddEntry(data_graph, label[0], "EP")
    for i in range(len(graph)):
        legend.AddEntry(graph[i], label[i+1], "l")
    legend.Draw()

    fig.canvas.Write()
