import math
import glob
import ROOT as root
import Base
import numpy as np
import csv
import copy
# This is used for testing the network on real data, we do not know the labels here

def get_data_test(inp):
    sqrtsMeV = 13.e6
    data = []
    fin = root.TFile(inp, "read")
    tin = fin.Get("tree")
    n_entries = tin.GetEntries()
    print(f"The original number of events for tesing: {n_entries}")
    for i in range(n_entries):
        
        match_n = 0 #0,1,2
        match_a = False
        match_c = False
        tin.GetEntry(i)
        #if  (i % 1000) == 0:
        #    print(i)
        DiphotonP4 = tin.PhotonP4[0] + tin.PhotonP4[1]
        phxia = DiphotonP4.M() / sqrtsMeV * math.exp(DiphotonP4.Rapidity())
        phxic = DiphotonP4.M() / sqrtsMeV * math.exp(-DiphotonP4.Rapidity())
        if (DiphotonP4.M() > 1600*1000 or DiphotonP4.M() < 150*1000):
            continue
        ProtonN, prxia, prxic, DeltaXiA, DeltaXiC = Base.slimProton(phxia, phxic, tin.ProtonN, tin.ProtonXi, tin.ProtonAC)
        Dphi = root.TVector2.Phi_mpi_pi(
            tin.PhotonP4[0].Phi() - tin.PhotonP4[1].Phi())
        Acoplanarity = 1 - abs(Dphi)/math.pi
        #delta xi cut -> acop, prxia, prxic, phxia, phxic
        # print(prxia,prxic)
        if (prxia > 0.035 and prxia < 0.08):
            match_a = True
        if (prxic > 0.035 and prxic < 0.08):
            match_c = True
        if match_c:
            if (abs(prxic - phxic) < (0.004 + 0.1*phxic)):
                match_n+=1
        if match_a:
            if (abs(prxia - phxia) < (0.004 + 0.1*phxia)):
                match_n+=1
        if not (match_n > 0):
             #print(match_n)
             continue
        data.append([tin.PhotonN, Acoplanarity, tin.PhotonP4[0].Eta(),
                    tin.PhotonP4[1].Eta(), tin.PhotonP4[0].Pt(), tin.PhotonP4[1].Pt()])
    print(f"The cut number of events for tesing: {len(data)}")
    return np.array(data)


def get_data_labels(sigs, bg):
    labels = []
    data = []
    for idx, sig in enumerate(sigs):
        num = len(sigs) - idx
        print(f"signals left: {num}\n")
        print(f"signal used: {sig}\n")
        # define signal and background NTuples
        fins = root.TFile(sig, "read")
        tins = fins.Get("tree")
        finb = root.TFile(bg, "read")
        tinb = finb.Get("tree")
        n_entries = tins.GetEntries()
        #print(n_entries,tinb.GetEntries())
        # signal data
        #print(n_entries)
        for i in range(n_entries):
            tins.GetEntry(i)
            Dphi = root.TVector2.Phi_mpi_pi(
            tins.PhotonP4[0].Phi() - tins.PhotonP4[1].Phi())
            Acoplanarity = 1 - abs(Dphi)/math.pi
            data.append([tins.PhotonN, Acoplanarity, tins.PhotonP4[0].Eta(
            ), tins.PhotonP4[1].Eta(), tins.PhotonP4[0].Pt(), tins.PhotonP4[1].Pt()])
            labels.append(1)
            # print("Number of photons: ",tins.PhotonN," Acoplanarity: ",Acoplanarity," Eta Photons: ",tins.PhotonP4[0].Eta(), tins.PhotonP4[1].Eta(), "Pt Photons: ", tins.PhotonP4[0].Pt(), tins.PhotonP4[1].Pt())
    old_n = n_entries
    n_entries = tinb.GetEntries()
    print(f'Number of BG events: {n_entries}\n Number of Signal Events: {old_n}')
    # background data
    for i in range(n_entries):
        tinb.GetEntry(i) 
        
        DiphotonP4 = tinb.PhotonP4[0] + tinb.PhotonP4[1]
        if (DiphotonP4.M() > 1600*1000 or DiphotonP4.M() < 150*1000):
            continue
        Dphi = root.TVector2.Phi_mpi_pi(
        tinb.PhotonP4[0].Phi() - tinb.PhotonP4[1].Phi())
        Acoplanarity = 1 - abs(Dphi)/math.pi
        data.append([tinb.PhotonN, Acoplanarity, tinb.PhotonP4[0].Eta(
        ), tinb.PhotonP4[1].Eta(), tinb.PhotonP4[0].Pt(), tinb.PhotonP4[1].Pt()])
        labels.append(0)
        # print("Number of photons: ",tinb.PhotonN," Acoplanarity: ",Acoplanarity," Eta Photons: ",tinb.PhotonP4[0].Eta(), tinb.PhotonP4[1].Eta(), "Pt Photons: ", tinb.PhotonP4[0].Pt(), tinb.PhotonP4[1].Pt())

    # Randomize the order of the data
    data = np.array(data)
    labels = np.array(labels)
    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    data = data[indices]
    labels = labels[indices]
    return data, labels


if __name__ == '__main__':
    signal_files = glob.glob("ALP_*_AFII_m*_g200_HIGG1D1.root")
    sig_copy = copy.deepcopy(signal_files)  
    #for i in range(8):
    #    for signal in sig_copy:
    #        if "m200_" in signal:
    #            print("double weight: ",signal)
    #            signal_files.append(signal)
    #signal_files = signal_files[0:1]#"data.root"#[0:1]#:2]
    # signal_files = ["ALP_DD_AFII_m1600_g200_HIGG1D1.root"]# ["ALP_DD_AFII_m400_g200_HIGG1D1.root"]
    background_file = "data.root"#"unified_sel3_a0-0.01_m1110_xyc0.031-0.084_xp0.035-0.08_dxc0.004_multishift_data.root"
    recorded = get_data_test(background_file)
    header = ['PhotonN', 'Acoplanarity', 'Eta0', 'Eta1', 'Pt0', 'Pt1'] 
    filename = 'recorded_data.csv'#recorded_data.csv'
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(recorded)
    print("recorded data saved\n")
    data_tr, labels_tr = get_data_labels(signal_files, background_file)
    labels = []
    for i in range(len(labels_tr)):
        labels.append([labels_tr[i]])
    #test_file =  "ALP_EL_AFII_m1000_g200_HIGG1D1.root"
    #data = get_data_test(test_file)
    datapr = 0.7
    ev_num = len(data_tr)
    thr = int(datapr*ev_num)
 
    data_test = data_tr[thr:ev_num]
    data_tr = data_tr[0:thr]
    labels_test = labels[thr:ev_num]
    labels_tr = labels[0:thr]
    # print(ev_num)
    #print(len(labels_test))
    #print(len(labels_tr))

    filename = 'train_data.csv'
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(data_tr)

    filename = 'train_labels.csv'
    header = ['labels']
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(labels_tr)
    
    filename = 'test_data.csv'
    header = ['PhotonN', 'Acoplanarity', 'Eta0', 'Eta1', 'Pt0', 'Pt1']
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(data_test)

    filename = 'test_labels.csv'
    header = ['labels']
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(labels_test)
    
