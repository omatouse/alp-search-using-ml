import ROOT as root
import atlasplots as aplt
import math, sys
import Base
import tqdm

from pylab import *
from scipy.optimize import leastsq

root.gROOT.SetBatch(True)

aplt.set_atlas_style()

#sel3_a0-0.01_m1110_xyc0.031-0.084_xp0.035-0.08_dxc0.004_data_looseIso.root
fin_after = root.TFile('data.root', "read")
tin_after = fin_after.Get("tree")
fout = root.TFile('canvas.root', "recreate")

sqrtsMeV = 13.e6
nbins, xl, xh = 50, 130, 1600
h_after = root.TH1D("after", "", nbins, xl, xh)

for ev in tin_after:
    DiphotonP4 = ev.PhotonP4[0] + ev.PhotonP4[1]
    #print(DiphotonP4.M())
    if ((DiphotonP4.M() < 150 * 1000) or (DiphotonP4.M() > 1600 * 1000)):
        continue
    DiphotonXiA = DiphotonP4.M() / sqrtsMeV * math.exp(DiphotonP4.Rapidity())
    DiphotonXiC = DiphotonP4.M() / sqrtsMeV * math.exp(-DiphotonP4.Rapidity())
    ProtonN, ProtonXiA, ProtonXiC, DeltaXiA, DeltaXiC = Base.slimProton(DiphotonXiA, DiphotonXiC, ev.ProtonN, ev.ProtonXi, ev.ProtonAC)
    h_after.Fill(DiphotonP4.M() * 1e-3, ev.Weight)

maximum = max([h.GetMaximum() + h.GetBinError(h.GetMaximumBin()) for h in [h_after]])

Base.plotSingleHist(h_after,pdfname="myy.pdf", xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_after, "GeV"), errorband = True, ylogscale=False)
#Base.plotSingleHist(h_origin, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_origin, "GeV"), errorband = True, ylim = [0, maximum], ylogscale=True)
#Base.plotSingleHist(h_before, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_before, "GeV"), errorband = True, ylim = [0, maximum], ylogscale=True)
#Base.plotSingleHist(h_after, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_after, "GeV"), errorband = True, ylim = [0, maximum], ylogscale=True)
#Base.plotSingleHist(h_origin, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_origin, "GeV"), errorband = False, ylim = [0, maximum])
#Base.plotSingleHist(h_before, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_before, "GeV"), errorband = False, ylim = [0, maximum])
#Base.plotSingleHist(h_after, xlabel = "Diphoton mass m_{#gamma#gamma} [GeV]", ylabel = Base.getYLabel(h_after, "GeV"), errorband = False, ylim = [0, maximum])
