import numpy as np
import matplotlib.pyplot as plt

# Define the sigmoid, relu, and tanh functions
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def relu(x):
    return np.maximum(0, x)

def tanh(x):
    return np.tanh(x)

# Set the interval for x
x = np.linspace(-3, 3, 1000)

# Calculate the y values for each function
y_sigmoid = sigmoid(x)
y_relu = relu(x)
y_tanh = tanh(x)

# Plot the functions
plt.plot(x, y_sigmoid, '-.', label='sigmoid')
plt.plot(x, y_relu, label='relu')
plt.plot(x, y_tanh, '--', label='tanh')

# Set the plot title and labels
plt.title('Sigmoid, ReLU, and Tanh Functions')
plt.xlabel('x')
plt.ylabel('y')

# Set the legend
plt.legend()
plt.savefig("activ_fun.png")
