activ_root.py, activation_plots.py, plot_root.py, bisection.py, plot_features.py confusion_matrix.py are plotting scripts.
threelayer.py is the base neural network with .pth weights being:
weights_4/8times.pth - enhanced sensitivity in lower masses.
weights_200.pth only trained on 200GeV
weights_og_w.pth trained on all data + not enhanced sensitivity
root2csv exports data from root into csv format
event_efficiency.py plots eff/sig plots
event_thresh.py plots dependancy of events on NN threshold
f_imp.py plots feature_importances using SHAP values
recorded_efficiency.py gets using bisection the correct threshold for event_efficiency.py