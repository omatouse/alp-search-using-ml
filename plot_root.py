import ROOT as root
import numpy as np
import array
import time
from scipy.interpolate import CubicSpline

def plot_graph(x_ax,y_ax,y_ax1,name):  
    x_ax = np.array(x_ax).astype(int)
    y_ax = np.array(y_ax)
    y_ax1 = np.array(y_ax1)
    indices = np.argsort(x_ax)
    x_ax = x_ax[indices]
    y_ax = y_ax[indices]
    y_ax1 = y_ax1[indices]
    plt.cla()
    plt.clf()
    plt.plot(x_ax, y_ax,'g-',label='Linear interp. network')
    plt.plot(x_ax, y_ax1,'m-',label='Linear interp. cut based')
    plt.scatter(x_ax, y_ax,marker='o',label='Data network')
    plt.scatter(x_ax, y_ax1,marker='s',label='Data cut based')
    cs = CubicSpline(x_ax,y_ax)
    cs1 = CubicSpline(x_ax,y_ax1)
    c_x = np.arange(200,1600)
    c_y = cs(c_x)
    c_y1 = cs1(c_x)
    plt.plot(c_x,c_y,'r--',label='Cubic interp. network')
    plt.plot(c_x,c_y1,'y--',label='Cubic interp. cut based')
    plt.xlabel("${m_x}$ [GeV]")
    if "sign" in name:
        plt.ylabel("Significance")
    else:
        plt.ylabel("Efficiency")
    plt.legend(fontsize="10")
    plt.savefig(name,bbox_inches='tight')

def plot_root(x,y,y1,name):
    x = np.array(x).astype(int)
    y = np.array(y)
    y1 = np.array(y1)
    indices = np.argsort(x)
    x  = x[indices]
    y = y[indices]
    y1 = y1[indices]
    x = array.array('d', x)
    y = array.array('d', y)
    y1 = array.array('d', y1)
    
    cs = CubicSpline(x,y)
    cs1 = CubicSpline(x,y1)
    c_x = np.arange(200,1600)
    c_y = cs(c_x)
    c_y1 = cs1(c_x)
    graph = root.TGraph(len(x), x, y) 
    c_x = array.array('d',c_x)
    c_y = array.array('d', c_y)
    c_y1 = array.array('d', c_y1)
    graph1 = root.TGraph(len(x), x, y1) 
    graph2 = root.TGraph(len(c_x), c_x, c_y) 
    graph3 = root.TGraph(len(c_x), c_x, c_y1) 
    spline = root.TSpline3("spline",graph)
    spline1 = root.TSpline3("spline",graph1) 
    #graph.SetTitle("My Graph")
    graph.GetXaxis().SetTitle("m_{x} [GeV]")
    graph.GetXaxis().SetTitleSize(0.05)
    graph.GetYaxis().SetTitleSize(0.05)
    graph.GetXaxis().SetLabelSize(0.05)
    graph.GetYaxis().SetLabelSize(0.05)
    if "Sign" in name:
        graph.GetYaxis().SetTitle("Significance")
        legend = root.TLegend(0.6,0.72, 0.99, 0.99)#0.6, 0.68, 0.9, 0.9)
    else:
        graph.GetYaxis().SetTitle("Efficiency")
        legend = root.TLegend(0.6, 0.15, 0.99, 0.42)
    # Set the line color and marker style
    graph.SetLineColor(root.kRed)
    graph1.SetLineColor(root.kYellow)
    spline.SetLineStyle(root.kDashed)
    spline1.SetLineStyle(root.kDashed)
    spline.SetLineColor(root.kGreen)
    spline1.SetLineColor(root.kBlue)

    canvas = root.TCanvas("canvas", "My Canvas")
    canvas.SetBottomMargin(0.15)
    canvas.SetLeftMargin(0.15)
    canvas.SetRightMargin(0.01)
    canvas.SetTopMargin(0.01)
    graph.SetTitle("")
    #graph.GetXaxis().SetRangeUser(0, 0.25)
    #graph1.GetXaxis().SetRangeUser(0, 0.25)

    graph.Draw("APL")
    graph.SetMarkerColor(root.kCyan)
    graph.SetMarkerStyle(root.kFullCircle)
    graph.Draw("P SAME")
    graph1.Draw("PL SAME")
    graph1.SetMarkerStyle(21)
    graph.SetMarkerColor(root.kGreen)
    graph1.SetLineColor(root.kMagenta)
    graph1.Draw("P SAME")
    spline.Draw("SAME")
    spline1.Draw("SAME")
    #legend.SetTextSize(0.032)
    legend.SetTextSize(0.04)
    legend.AddEntry(graph, "Data network", "p")
    legend.AddEntry(graph1, "Data prev analysis", "p")
    legend.AddEntry(graph, "Lin interp network", "l")
    legend.AddEntry(graph1, "Lin interp prev analysis", "l")
    legend.AddEntry(spline, "Cubic interp network", "l")
    legend.AddEntry(spline1, "Cubic interp prev analysis", "l")
    legend.Draw()
    canvas.Draw()
    canvas.SaveAs(name)
  
def plot_thresh(x,y, y_thr):
    name = "event_threshold.png"
    x = np.array(x)
    y = np.array(y)
    x = array.array('d', x)
    y = array.array('d', y)
    graph = root.TGraph(len(x), x, y) 
    graph.GetXaxis().SetTitle("Threshold")
    graph.GetXaxis().SetTitleSize(0.05)
    graph.GetYaxis().SetTitleSize(0.05)
    graph.GetXaxis().SetLabelSize(0.05)
    graph.GetYaxis().SetLabelSize(0.05)
    legend = root.TLegend(0.69,0.77, 0.99, 0.99)#0.6, 0.68, 0.9, 0.9)
    graph.GetYaxis().SetTitle("Number of background events")
    #legend = root.TLegend(0.69, 0.15, 0.99, 0.37)
    # Set the line color and marker style
    graph.SetLineColor(root.kBlack)
    graph.SetLineWidth(3)

    canvas = root.TCanvas("canvas", "My Canvas")
    canvas.SetBottomMargin(0.15)
    canvas.SetLogy()
    canvas.SetLeftMargin(0.15)
    canvas.SetRightMargin(0.01)
    canvas.SetTopMargin(0.01)
    graph.SetTitle("")
    #graph.GetXaxis().SetRangeUser(0, 0.25)
    #graph1.GetXaxis().SetRangeUser(0, 0.25)

    graph.Draw("AL")
    graph.SetMarkerColor(root.kCyan)
    #legend.SetTextSize(0.032)
    #legend.AddEntry(graph, "", "l")
    #legend.Draw()

    line = root.TLine(graph.GetXaxis().GetXmin(), y_thr, graph.GetXaxis().GetXmax(), y_thr)
    line.SetLineStyle(2)  # Dotted line
    line.SetLineColor(root.kRed)  # Red color

    line.Draw()

    canvas.Draw()
    canvas.SaveAs(name)
  
def plot_roc(x,y):
    name = "roc.png"
    x = np.array(x)
    y = np.array(y)
    x = array.array('d', x)
    y = array.array('d', y)
    graph = root.TGraph(len(x), x, y) 
    graph.GetXaxis().SetTitle("False positive rate")
    graph.GetXaxis().SetTitleSize(0.05)
    graph.GetYaxis().SetTitleSize(0.05)
    graph.GetXaxis().SetLabelSize(0.05)
    graph.GetYaxis().SetLabelSize(0.05)
    legend = root.TLegend(0.69,0.77, 0.99, 0.99)#0.6, 0.68, 0.9, 0.9)
    graph.GetYaxis().SetTitle("True positive rate")
    #legend = root.TLegend(0.69, 0.15, 0.99, 0.37)
    # Set the line color and marker style
    graph.SetLineColor(root.kBlack)
    graph.SetLineWidth(3)
    graph.GetXaxis().SetRangeUser(-0.1, 1.);
    canvas = root.TCanvas("canvas", "My Canvas")
    canvas.SetBottomMargin(0.15)
    canvas.SetLeftMargin(0.15)
    canvas.SetRightMargin(0.01)
    canvas.SetTopMargin(0.01)
    graph.SetTitle("")
    #graph.GetXaxis().SetRangeUser(0, 0.25)
    #graph1.GetXaxis().SetRangeUser(0, 0.25)

    graph.Draw("AL")
    graph.SetMarkerColor(root.kCyan)
    #legend.SetTextSize(0.032)
    #legend.AddEntry(graph, "", "l")
    #legend.Draw()
    canvas.Draw()
    canvas.SaveAs(name)
  
