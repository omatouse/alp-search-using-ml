import ROOT
import numpy as np
import copy
def gen_hist(x,name,eta0 = False, eta1 = False, pt1 = False, pt0=False):

    if pt0:
        hist = ROOT.TH1F(name[0],name[1], 100, 25, 420)
    elif pt1:
        hist = ROOT.TH1F(name[0],name[1], 100, 25, 370)
    elif eta0:
        hist = ROOT.TH1F(name[0],name[1], 100, -2.5, 2.5)
    elif eta1:
        hist = ROOT.TH1F(name[0],name[1], 100, -2.5, 2.5)
    else:
        hist = ROOT.TH1F(name[0],name[1], 100, -1, 1)
    hist.SetTitle("")
    hist.GetXaxis().SetTitle(x)
    hist.GetYaxis().SetTitle("Events")
    hist.GetXaxis().SetLabelSize(0.05)
    hist.GetYaxis().SetLabelSize(0.05)
    hist.GetXaxis().SetTitleSize(0.05)
    hist.GetYaxis().SetTitleSize(0.05)

    hist.SetStats(False)
    return copy.deepcopy(hist)
# Open the ROOT file containing the ntuple
file = ROOT.TFile("ALP_DD_AFII_m700_g200_HIGG1D1.root")

# Get the ntuple from the file
ntuple = file.Get("tree")
hist = gen_hist("Acoplanarity",("hist","myhist"))
hist.SetLineColor(ROOT.kGreen)
hist0pt = copy.deepcopy(gen_hist("Leading photon transverse momentum [GeV]",("hist","myhist"),pt0=True))
hist0eta = copy.deepcopy(gen_hist("Leading photon pseudorapidity",("hist","myhist"),eta0=True))
hist1pt = copy.deepcopy(gen_hist("Subleading photon transverse momentum [GeV]",("hist","myhist"),pt1=True))
hist1eta = copy.deepcopy(gen_hist("Subleading photon pseudorapidity",("hist","myhist"),eta1=True))
hist0pt.SetLineColor(ROOT.kGreen)
hist0eta.SetLineColor(ROOT.kGreen)
hist1eta.SetLineColor(ROOT.kGreen)
hist1pt.SetLineColor(ROOT.kGreen)

hist0pt1 = copy.deepcopy(gen_hist("Leading photon transverse momentum [GeV]",("hist","myhist"),pt0=True))
hist0eta1 = copy.deepcopy(gen_hist("Leading photon pseudorapidity",("hist","myhist"),eta0=True))
hist1pt1 = copy.deepcopy(gen_hist("Subleading photon transverse momentum [GeV]",("hist","myhist"),pt1=True))
hist1eta1 = copy.deepcopy(gen_hist("Subleading photon pseudorapidity",("hist","myhist"),eta1=True))
hist0eta1.SetLineColor(ROOT.kRed)
hist0pt1.SetLineColor(ROOT.kRed)
hist1eta1.SetLineColor(ROOT.kRed)
hist1pt1.SetLineColor(ROOT.kRed)
x = []
# Loop over the ntuple entries and fill the histogram
y = []
for idx, event in enumerate(ntuple):
    ntuple.GetEntry(idx)
    Dphi = (
    ntuple.PhotonP4[0].Phi() - ntuple.PhotonP4[1].Phi())
    Acoplanarity = 1 - abs(Dphi)/np.pi
    hist.Fill(Acoplanarity)
    hist0pt.Fill(ntuple.PhotonP4[0].Pt()/1000)
    hist0eta.Fill(ntuple.PhotonP4[0].Eta())
    hist1pt.Fill(ntuple.PhotonP4[1].Pt()/1000)
    hist1eta.Fill(ntuple.PhotonP4[1].Eta())
    x.append(Acoplanarity)
#hist.Draw("AP")
hst = copy.deepcopy(hist)
file = ROOT.TFile("data.root")#ALP_SD_AFII_m1000_g200_HIGG1D1.root")
hist0pt= copy.deepcopy(hist0pt)
hist1pt = copy.deepcopy(hist1pt)
hist0eta = copy.deepcopy(hist0eta)
hist1eta = copy.deepcopy(hist1eta)

# Get the ntuple from the file
ntuple = file.Get("tree")
hist1 = gen_hist("Acoplanarity",("hist1","myhist1"))
hist1.SetLineColor(ROOT.kRed)
#print(hist1)# Create a TH1F object
for idx, event in enumerate(ntuple):
    ntuple.GetEntry(idx)
    Dphi = (
    ntuple.PhotonP4[0].Phi() - ntuple.PhotonP4[1].Phi())
    Acoplanarity = 1 - abs(Dphi)/np.pi
    hist1.Fill(Acoplanarity)
    hist0pt1.Fill(ntuple.PhotonP4[0].Pt()/1000)
    hist0eta1.Fill(ntuple.PhotonP4[0].Eta())
    hist1pt1.Fill(ntuple.PhotonP4[1].Pt()/1000)
    hist1eta1.Fill(ntuple.PhotonP4[1].Eta())
    y.append(Acoplanarity)

hst.Scale(1.0/hst.Integral())
hist1.Scale(1.0/hist1.Integral())

canvas = ROOT.TCanvas("canvas", "My Canvas")
legend = ROOT.TLegend(0.6, 0.9, 0.99, 0.99) # set legend position
legend.SetTextSize(0.04)
legend.AddEntry(hst,"Normalised Signal",'l')
legend.AddEntry(hist1,"Normalised background",'l')
canvas.SetBottomMargin(0.15)
canvas.SetLeftMargin(0.15)
canvas.SetRightMargin(0.01)
canvas.SetTopMargin(0.01)
hst.Draw("HIST")
hist1.Draw("HIST SAME")
legend.Draw()
canvas.SaveAs("feature_plots/aco.png")

canvas.SetRightMargin(0.03)
canvas.SetTopMargin(0.03)
hist0pt.Scale(1.0/hist0pt.Integral())
legend = ROOT.TLegend(0.51, 0.88, 0.9, 0.97) # set legend position
legend = ROOT.TLegend(0.58, 0.88, 0.97, 0.97) # set legend position
legend.SetTextSize(0.04)
legend.AddEntry(hst,"Normalised Signal",'l')
legend.AddEntry(hist1,"Normalised background",'l')
hist0pt1.Scale(1.0/hist0pt1.Integral())

hist0pt1.Draw("HIST")
hist0pt.Draw("HIST SAME")
legend.Draw()
canvas.SaveAs("feature_plots/0pt.png")

canvas.SetRightMargin(0.03)
hist1pt.Scale(1.0/hist1pt.Integral())
legend = ROOT.TLegend(0.58, 0.88, 0.97, 0.97) # set legend position
legend.SetTextSize(0.04)
legend.AddEntry(hst,"Normalised Signal",'l')
legend.AddEntry(hist1,"Normalised background",'l')
hist1pt1.Scale(1.0/hist1pt1.Integral())
hist1pt1.Draw("HIST")
hist1pt.Draw("HIST SAME")
legend.Draw()
canvas.SaveAs("feature_plots/1pt.png")

canvas.SetRightMargin(0.03)
hist0eta.Scale(1.0/hist0eta.Integral())
hist0eta1.Scale(1.0/hist0eta1.Integral())
legend = ROOT.TLegend(0.58, 0.88, 0.97, 0.97) # set legend position
legend.SetTextSize(0.04)
legend.AddEntry(hst,"Normalised Signal",'l')
legend.AddEntry(hist1,"Normalised background",'l')
hist0eta.Draw("HIST")
hist0eta1.Draw("HIST SAME")
legend.Draw()
canvas.SaveAs("feature_plots/0eta.png")

hist1eta.Scale(1.0/hist1eta.Integral())
hist1eta1.Scale(1.0/hist1eta1.Integral())
canvas.SetRightMargin(0.03)

legend = ROOT.TLegend(0.58, 0.88, 0.97, 0.97) # set legend position
legend.SetTextSize(0.04)
legend.AddEntry(hst,"Normalised Signal",'l')
legend.AddEntry(hist1,"Normalised background",'l')
hist1eta.Draw("HIST")
hist1eta1.Draw("HIST SAME")
legend.Draw()
canvas.SaveAs("feature_plots/1eta.png")
