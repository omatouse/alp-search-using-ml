import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
tn = 16304
tp = 169563
fn = 4589
fp = 4553




data = {
"Signal predicted":  [tp / (tp+fn), fp / (tn+fp)],
"Background predicted": [fn / (tp+fn), tn / (tn+fp)]
}
data = {
"Signal predicted":  [0.92,0.053],
"Background predicted": [0.082,0.95]
} 
df = pd.DataFrame(data, index=["Real signal", "Real background"])
conf = sns.heatmap(df, annot=True, annot_kws={"size": 14}, cmap="Blues")
conf.set_xticklabels(conf.get_xticklabels(), fontsize=14)
conf.set_yticklabels(conf.get_yticklabels(), fontsize=14)
plt.savefig("conf.png")
