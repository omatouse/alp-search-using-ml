import ROOT as root
import math, sys, glob
import Base
import tqdm

output_cutflow = True
display_progress_bar = False
correction = 0.1


if output_cutflow:
    import atlasplots as aplt
    root.gROOT.SetBatch(True)
    aplt.set_atlas_style()

def main():
    ifname = glob.glob("ALP_SD_AFII_m1000_g200_HIGG1D1.root")
    #ifname = [sys.argv[1]]
    #ifname = glob.glob("LBshift1_data_looseIso.root")
    #ifname = ["data_oneTight.root"]
    #ifname = ["mixmc_el2tot_ALP_EL_AFII_m400_g200_HIGG1D1.root", "el2tot_ALP_EL_AFII_m400_g200_HIGG1D1.root"]
    sellevel = [3] # 0-3
    acopCut = [[0, 0.01]]
    #acopCut = [[0, 0.01], [0, 0.02], [0, 0.03], [0, 0.04], [0, 0.05]]
    mcondition = [[True, True, True, False]]
    xiyyCut = [[0.031, 0.084]]
    xiyyCutCorrection = [True]
    xiAFPCut = [[0.035, 0.08]]
    DXiCut = [0.004]
    DXiCutCorrection = [True]
    for i in ifname:
        for s in sellevel:
            if s < 2:
                selection(i, s)
            else:
                for a in acopCut:
                    if s < 3:
                        selection(i, s, a)
                    else:
                        for xy in xiyyCut:
                            for xyc in xiyyCutCorrection:
                                for xp in xiAFPCut:
                                    for dx in DXiCut:
                                        for dxc in DXiCutCorrection:
                                            for m in mcondition:
                                                selection(i, s, a, xy, xyc, xp, dx, dxc, m)

def selection(ifname, sellevel, acopCut=False, xiyyCut=False, xiyyCutCorrection=True, xiAFPCut=False, DXiCut=False, DXiCutCorrection=True, mcondition=[]):

    print("Reading " + ifname)
    sqrtsMeV = 13.e6

    foutname = 'sel' + str(sellevel)
    if sellevel >= 2:
        if (sellevel == 2 and not acopCut):
            print("ERROR: Acoplanarity condition is not specified even though you set sellevel as 2")
            sys.exit(1)
        if acopCut:
            foutname += ("_a" + "-".join([str(x) for x in acopCut]))
        if sellevel >= 3:
            if (not DXiCut) and not (DXiCut is False):
                print("ERROR: Do not set DXiCut as 0")
                sys.exit(1)
            if not (xiyyCut or xiAFPCut or DXiCut):
                print("ERROR: No condition is set even though you set sellevel as 3")
                sys.exit(1)
            foutname += "_m"
            for c in mcondition:
                foutname += str(int(c))
            if xiyyCut:
                if xiyyCutCorrection:
                    if len(xiyyCut) != 2:
                        print("ERROR: Set length of xiyyCut as 2 when you apply the threshold correction.")
                        sys.exit(1)
                    xiyyCutCorrected = [xiyyCut[0] / (1+correction), xiyyCut[1] / (1-correction)]
                    foutname += ("_xyc" + "-".join([str(x) for x in xiyyCut]))
                else:
                    xiyyCutCorrected = list(xiyyCut)
                    foutname += ("_xy" + "-".join([str(x) for x in xiyyCut]))
            else:
                xiyyCutCorrected = list(xiyyCut)
            if xiAFPCut:
                foutname += ("_xp" + "-".join([str(x) for x in xiAFPCut]))
            if DXiCut:
                if DXiCutCorrection:
                    foutname += ("_dxc" + str(DXiCut))
                else:
                    foutname += ("_dx" + str(DXiCut))

    foutname += f"_{ifname[:-5]}.root"

    fin = root.TFile(ifname, "read")
    tin = fin.Get("tree")
    nEntries = tin.GetEntries()
    fout = root.TFile(foutname, "recreate")
    tout = tin.CloneTree(0)
    if output_cutflow:
        h_cutflow_a = root.TH1D("cutflow_a", "", 6, 0, 6)
        h_cutflow_c = root.TH1D("cutflow_c", "", 6, 0, 6)
        h_cutflow_and = root.TH1D("cutflow_and", "", 6, 0, 6)
        h_cutflow_or = root.TH1D("cutflow_or", "", 6, 0, 6)
    
    print(f"Filling {foutname}")
    nselected = 0
    for i in tqdm.tqdm(range(nEntries)) if display_progress_bar else range(nEntries):
        tin.GetEntry(i)

        if sellevel < 1:
            nselected += 1
            tout.Fill()
            continue

        if output_cutflow:
            h_cutflow_a.Fill(0, tin.Weight)
            h_cutflow_c.Fill(0, tin.Weight)
            h_cutflow_and.Fill(0, tin.Weight)
            h_cutflow_or.Fill(0, tin.Weight)
        if tin.PhotonN < 2:
            continue
        if output_cutflow:
            h_cutflow_a.Fill(1, tin.Weight)
            h_cutflow_c.Fill(1, tin.Weight)
            h_cutflow_and.Fill(1, tin.Weight)
            h_cutflow_or.Fill(1, tin.Weight)

        if sellevel < 2:
            nselected += 1
            tout.Fill()
            continue
        Dphi = root.TVector2.Phi_mpi_pi(tin.PhotonP4[0].Phi() - tin.PhotonP4[1].Phi())
        Acop = 1 - abs(Dphi) / math.pi
        if acopCut and not Base.satisfyRegion(Acop, acopCut):
            continue
        if output_cutflow:
            h_cutflow_a.Fill(2, tin.Weight)
            h_cutflow_c.Fill(2, tin.Weight)
            h_cutflow_and.Fill(2, tin.Weight)
            h_cutflow_or.Fill(2, tin.Weight)

        if sellevel < 3:
            nselected += 1
            tout.Fill()
            continue
        DiphotonP4 = tin.PhotonP4[0] + tin.PhotonP4[1]
        if (DiphotonP4.M() < 150*1000 or DiphotonP4.M() > 1600*1000):
            continue
        DiphotonXiA = DiphotonP4.M() / sqrtsMeV * math.exp(DiphotonP4.Rapidity())
        DiphotonXiC = DiphotonP4.M() / sqrtsMeV * math.exp(-DiphotonP4.Rapidity())
    
        matchedA, matchedC = True, True
    
        ProtonN, ProtonXiA, ProtonXiC, DeltaXiA, DeltaXiC = Base.slimProton(DiphotonXiA, DiphotonXiC, tin.ProtonN, tin.ProtonXi, tin.ProtonAC)
        if xiyyCutCorrected:
            matchedA = Base.satisfyRegion(DiphotonXiA, xiyyCutCorrected)
            matchedC = Base.satisfyRegion(DiphotonXiC, xiyyCutCorrected)
        if output_cutflow and (matchedA or matchedC):
            h_cutflow_or.Fill(3, tin.Weight)
            if matchedA:
                h_cutflow_a.Fill(3, tin.Weight)
            if matchedC:
                h_cutflow_c.Fill(3, tin.Weight)
                if matchedA:
                    h_cutflow_and.Fill(3, tin.Weight)
        
        if xiAFPCut:
            if matchedA:
                matchedA = (ProtonXiA >= 0 and Base.satisfyRegion(ProtonXiA, xiAFPCut))
            if matchedC:
                matchedC = (ProtonXiC >= 0 and Base.satisfyRegion(ProtonXiC, xiAFPCut))
        if output_cutflow and (matchedA or matchedC):
            h_cutflow_or.Fill(4, tin.Weight)
            if matchedA:
                h_cutflow_a.Fill(4, tin.Weight)
            if matchedC:
                h_cutflow_c.Fill(4, tin.Weight)
                if matchedA:
                    h_cutflow_and.Fill(4, tin.Weight)

        if DXiCut:
            if matchedA:
                matchedA = (ProtonXiA >= 0 and abs(ProtonXiA - DiphotonXiA) < ((DXiCut + correction * DiphotonXiA) if DXiCutCorrection else DXiCut))
            if matchedC:
                matchedC = (ProtonXiC >= 0 and abs(ProtonXiC - DiphotonXiC) < ((DXiCut + correction * DiphotonXiC) if DXiCutCorrection else DXiCut))
        if output_cutflow and (matchedA or matchedC):
            h_cutflow_or.Fill(5, tin.Weight)
            if matchedA:
                h_cutflow_a.Fill(5, tin.Weight)
            if matchedC:
                h_cutflow_c.Fill(5, tin.Weight)
                if matchedA:
                    h_cutflow_and.Fill(5, tin.Weight)
            
        if not Base.satisfyMatchingCondition(mcondition, matchedA, matchedC):
            continue
        
        nselected += 1
        tout.Fill()
    
    print(str(nselected) + " events selected")
    tout.AutoSave()

    if output_cutflow:
        Base.plotMultiHist([h_cutflow_a, h_cutflow_c, h_cutflow_and, h_cutflow_or], ["A-side", "C-side", "AND", "OR"], xlabel = "Condtion", ylabel = "Events", canvname = "cut_flow")
main()
