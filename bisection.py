import ROOT
import numpy as np

# Generate a nonlinear function f(x)
def f(x):
    return np.sin(x) + x**3 - 2*x - 1

# Define the starting interval
x1 = 0.5
x2 = 1.5

# Calculate the middle point
x3 = (x1 + x2) / 2

# Evaluate f(x) at the starting interval and the middle point
y1 = f(x1)
y2 = f(x2)
y3 = f(x3)

# Create a range of x values to plot the function
x_vals = np.linspace(x1-0.5, x2+0.5, 1000)
y_vals = [f(x) for x in x_vals]

# Create a TGraph object with the function values
graph = ROOT.TGraph(len(x_vals), np.array(x_vals), np.array(y_vals))

# Set the axis labels and title
graph.GetXaxis().SetTitle('x')
graph.GetYaxis().SetTitle('f(x)')
graph.GetXaxis().SetTitleSize(0.05)
graph.GetYaxis().SetTitleSize(0.05)
graph.GetXaxis().SetLabelSize(0.05)
graph.GetYaxis().SetLabelSize(0.05)

# Create marker objects for the starting interval and the middle point
marker_x1 = ROOT.TMarker(x1, y1, 20)
marker_x1.SetMarkerColor(ROOT.kRed)
marker_x1.SetMarkerSize(1.5)

marker_x2 = ROOT.TMarker(x2, y2, 20)
marker_x2.SetMarkerColor(ROOT.kBlue)
marker_x2.SetMarkerSize(1.5)

marker_x3 = ROOT.TMarker(x3, y3, 20)
marker_x3.SetMarkerColor(ROOT.kGreen)
marker_x3.SetMarkerSize(1.5)

# Draw the graph and the markers
canvas = ROOT.TCanvas('canvas', '')
canvas.SetTitle("")
canvas.SetMargin(0.1, 0.03, 0.1, 0.01)
graph.Draw('AL')
graph.SetTitle("")
marker_x1.Draw('same')
marker_x2.Draw('same')
marker_x3.Draw('same')

# Draw vertical lines at the starting interval and the middle point
line_x1 = ROOT.TLine(x1, graph.GetYaxis().GetXmin(), x1, graph.GetYaxis().GetXmax())
line_x1.SetLineColor(ROOT.kRed)
line_x1.SetLineStyle(2)
line_x1.SetLineWidth(2)
line_x1.Draw('same')

line_x2 = ROOT.TLine(x2, graph.GetYaxis().GetXmin(), x2, graph.GetYaxis().GetXmax())
line_x2.SetLineColor(ROOT.kBlue)
line_x2.SetLineStyle(2)
line_x2.SetLineWidth(2)
line_x2.Draw('same')

line_x3 = ROOT.TLine(x3, graph.GetYaxis().GetXmin(), x3, graph.GetYaxis().GetXmax())
line_x3.SetLineColor(ROOT.kGreen)
line_x3.SetLineStyle(2)
line_x3.SetLineWidth(2)
line_x3.Draw('same')

# Draw a horizontal line at y=0
zero_line = ROOT.TLine(graph.GetXaxis().GetXmin(), 0, graph.GetXaxis().GetXmax(), 0)
zero_line.SetLineColor(ROOT.kBlack)
zero_line.SetLineStyle(1)
zero_line.SetLineWidth(2)
zero_line.Draw('same')
# Add labels and legend to the plot
legend = ROOT.TLegend(0.65+0.18, 0.75+0.09, 0.97, 0.99)
legend.SetFillColor(ROOT.kWhite)
legend.SetTextSize(0.05)


legend.AddEntry(marker_x1, 'x_1', 'p')
legend.AddEntry(marker_x2, 'x_2', 'p')
legend.AddEntry(marker_x3, 'x_3', 'p')
legend.Draw()

canvas.Draw()
# Save the plot as a PNG image file
canvas.SaveAs("bisection_method.png")

