import numpy as np
import os
import math
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv


from sklearn.metrics import f1_score, roc_curve, auc
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv
import torchvision.transforms as tfms
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD
from torch.utils.data import Dataset
from plot_root import plot_roc

class Dataset_tr(Dataset):
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        label = self.labels[idx]
        return meas, label
    

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.first = nn.Sequential(
            nn.Linear(6, 16),
            nn.BatchNorm1d(16),
            nn.ReLU())

        self.second = nn.Sequential(
            nn.Linear(16, 4),
            nn.BatchNorm1d(4),
            nn.ReLU())

        self.third = nn.Sequential(
            nn.Linear(4, 2),
            nn.Softmax(dim=1))
	
        self.weight_init()

    def weight_init(self):
        for lay in self.modules():
            if type(lay) == torch.nn.Linear:
                torch.nn.init.xavier_uniform_(lay.weight)

    def forward(self, x):
        x = self.first(x)
        x = self.second(x)
        x = self.third(x)
        return x


def get_roc(model, val_loader, device):
    fprs = []
    tprs = []
    thresholds = []
    labels = np.array([])
    outputs = np.array([])
    model.eval()

    with torch.no_grad():
        for idx, batch in enumerate(val_loader):
            data, label = batch		#labels is actually only one label
            data = data.to(device)
            label = label.to(device)
            output = model(data.float())
            label = int(label) 
            #threshold value - now its 0.5
            output = float(output[0][1])
            labels = np.append(labels, label)
            outputs = np.append(outputs, output)
    fprs, tprs, thresholds = roc_curve(labels, outputs)
    opt = torch.tensor([0,1])
    max_j = 0
    opt_thresh = 0.5
    #we are finding the  maximum of  J=sensitivity+specificity-1 Youden's J statistic
    for i in range(len(tprs)):
        specificity = 1 - fprs[i] #TNR
        sensitivity = tprs[i] #TPR
        J = sensitivity + specificity-1
        if J > max_j:
            max_j = J
            opt_thresh = thresholds[i]
    #F1 scores
    scores = []
    thresh_sub = np.array([])
    for idx, t in enumerate(thresholds):
        if idx % 100 == 0:
            thresh_sub = np.append(thresh_sub,t)
    for idx, t in enumerate(thresh_sub):
        scores.append(f1_score(labels, outputs >= t))
    scores = np.array(scores)
    opt_idx = np.argmax(scores)*100 + 100
    opt = thresholds[opt_idx]
    print(f'f1 says opt is {opt}, fpr is: {fprs[opt_idx]}, tprs is: {tprs[opt_idx]}')
    return tprs, fprs, opt_thresh


def get_device():
    if torch.cuda.is_available():
        gpu = get_free_gpu()
        print("Found GPU\n")
        device = torch.device(gpu)
    else:
        device = 'cpu'
    return device


def load_model():
    model = Model().to(device)
    model.load_state_dict(torch.load(
        f'weights.pth', map_location=torch.device('cpu')))
    return model


def csv2arr(filename,labels=False):
    #Labels=True says, that the file contains labels
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    if labels:
        ctr_s = 0
        ctr_b = 0
        for row in csvreader:
            if int(row[0]) == 0:
                 ctr_b += 1
            elif int(row[0]) == 1:
                 ctr_s += 1
            else:
                 print(row)
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
        file.close()
        return np.array(rows), header, ctr_s, ctr_b
    else:
        for row in csvreader:
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
    file.close()
    return np.array(rows), header


def transform(data):
    mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
    std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]
    return (data-mean)/std

if __name__ == '__main__':

    BATCH_SIZE = 64
    NUM_WORKERS = 10
    datapr = 0.8  # TR to VAL proportions
    data, _ = csv2arr('test_data.csv')
    labels, _, _, _ = csv2arr('test_labels.csv',True)
    train_thresh = int(datapr*len(data))
    data = transform(data)
    val_dataset = Dataset_tr(torch.from_numpy(data),torch.from_numpy(labels))#Dataset_tr(torch.from_numpy(data[train_thresh:len(data)]),torch.from_numpy(labels[train_thresh:len(data)]))
    val_dataloader = DataLoader(
        dataset=val_dataset, batch_size=1, shuffle=True)
    device = get_device()
    model = load_model()
    fpr = []
    tpr = []
    tpr, fpr, thr = get_roc(model, val_dataloader, device)
    plot_roc(fpr, tpr)
    plt.plot(fpr, tpr)
    print(f'J says that the optimal threshold is: {thr}')
    
    plt.xlabel("FPR")
    plt.ylabel("TPR")
    plt.savefig("rocplt.png",bbox_inches='tight')
    plt.show()
