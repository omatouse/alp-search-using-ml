import numpy as np
import os
import math
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv
import torchvision.transforms as tfms
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD
from torch.utils.data import Dataset

from plot_root import plot_thresh
class Dataset_test(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        return meas    

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.first = nn.Sequential(
            nn.Linear(6, 16),
            nn.BatchNorm1d(16),
            nn.ReLU())

        self.second = nn.Sequential(
            nn.Linear(16, 4),
            nn.BatchNorm1d(4),
            nn.ReLU())

        self.third = nn.Sequential(
            nn.Linear(4, 2),
            nn.Softmax(dim=1))
	
        self.weight_init()

    def weight_init(self):
        for lay in self.modules():
            if type(lay) == torch.nn.Linear:
                torch.nn.init.xavier_uniform_(lay.weight)

    def forward(self, x):
        x = self.first(x)
        x = self.second(x)
        x = self.third(x)
        return x


def validate_model(model, val_loader, device):
    model.eval()
    ev_num = 0
    outputs = []
    with torch.no_grad():
        for idx, batch in enumerate(val_loader):
            data  = batch		#labels is actually only one label
            data = data.to(device)
            output = model(data.float())
            #threshold value - now its 0.5
            output = float(output[0][1])
            outputs.append(output)
            #if output > thr:
            #    ev_num += 1
    return outputs


def get_device():
    if torch.cuda.is_available():
        gpu = get_free_gpu()
        print("Found GPU\n")
        device = torch.device(gpu)
    else:
        device = 'cpu'
    return device


def load_model():
    model = Model().to(device)
    model.load_state_dict(torch.load(
        f'weights.pth', map_location=torch.device('cpu')))
    return model


def csv2arr(filename,labels=False):
    #Labels=True says, that the file contains labels
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    if labels:
        ctr_s = 0
        ctr_b = 0
        for row in csvreader:
            if int(row[0]) == 0:
                 ctr_b += 1
            elif int(row[0]) == 1:
                 ctr_s += 1
            else:
                 print(row)
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
        file.close()
        return np.array(rows), header, ctr_s, ctr_b
    else:
        for row in csvreader:
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
    file.close()
    return np.array(rows), header

def transform(data):
    mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
    std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]
    return (data-mean)/std
def pred(preds,thr):
    ev_num = 0
    for ev in preds:
        if ev > thr:
            ev_num += 1
    return ev_num
if __name__ == '__main__':
    BATCH_SIZE = 64
    NUM_WORKERS = 10
    data, _ = csv2arr('recorded_data.csv')#alp_sd_m1000.csv')#'recorded_data.csv')
    #labels, _, _, _ = csv2arr('data_all_labels.csv',True)
    #train_thresh = int(datapr*len(data))
    data = transform(data)
    val_dataset = Dataset_test(torch.from_numpy(data))
    val_dataloader = DataLoader(
        dataset=val_dataset, batch_size=1, shuffle=True)
    device = get_device()
    model = load_model()
    x_ax = []
    y_ax = []
    preds  = validate_model(
    model, val_loader=val_dataloader, device=device)

    for i in range(101):
        thresh = (i)/100
        print(f"Evaluating at thresh {thresh}")
        ev_num = pred(preds,thresh)
        print(ev_num)
        x_ax.append(thresh)
        y_ax.append(ev_num)
    '''
    for i in range(101):
        thresh = 0.99 + (i)/10000
        print(f"Evaluating at thresh {thresh}")
        ev_num = pred(preds,thresh)
        print(ev_num)
        x_ax.append(thresh)
        y_ax.append(ev_num)
    '''
    plot_thresh(x_ax, y_ax, 441)
    plt.cla()
    plt.clf()
    plt.plot(x_ax, y_ax)
    plt.xlabel("Threshold")
    plt.ylabel("Number of background Events")
    #plt.yscale('log')
    #plt.xscale('log')
    plt.savefig("ev_thr_sigsd1000.png",bbox_inches='tight')
    plt.show()

