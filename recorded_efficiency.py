import copy
import numpy as np
import os
import math
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv
import glob
import re
import torch
import ROOT as root
import torch.nn as nn
import torch.nn.functional as F
import torchvision as tv
import torchvision.transforms as tfms
from scipy.interpolate import CubicSpline
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD
from torch.utils.data import Dataset
import Base

class Dataset_tr(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        meas = self.data[idx]
        return meas

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.first = nn.Sequential(
            nn.Linear(6, 16),
            nn.BatchNorm1d(16),
            nn.ReLU())

        self.second = nn.Sequential(
            nn.Linear(16, 4),
            nn.BatchNorm1d(4),
            nn.ReLU())

        self.third = nn.Sequential(
            nn.Linear(4, 2),
            nn.Softmax(dim=1))
	
        self.weight_init()

    def weight_init(self):
        for lay in self.modules():
            if type(lay) == torch.nn.Linear:
                torch.nn.init.xavier_uniform_(lay.weight)

    def forward(self, x):
        x = self.first(x)
        x = self.second(x)
        x = self.third(x)
        return x


def signal_passed(model, val_loader, device, thr):
    model.eval()
    beg = 0
    ev_goal = 441
    end = 1
    thr = 0.5
    ev_num = 0
    while(True):
        print(f'Events for thr: {thr} are {ev_num}')
        ev_num = 0
        with torch.no_grad():
            for idx, batch in enumerate(val_loader):
                data = batch
                data = data.to(device)
                output = model(data.float())
                output = float(output[0][1])
                if output > thr:
                    ev_num += 1
        if ev_num > ev_goal:
            beg = thr
            thr = thr + (end-beg)/2
        if ev_num < ev_goal:
            end = thr
            thr = thr - (end-beg)/2
    return ev_num


def get_device():
    if torch.cuda.is_available():
        gpu = get_free_gpu()
        print("Found GPU\n")
        device = torch.device(gpu)
    else:
        device = 'cpu'
    return device


def load_model(device): 
    model = Model()
    model = model.to(device)
    model.load_state_dict(torch.load(
        f'weights.pth', map_location=torch.device('cpu')))
    return model


def csv2arr(filename,labels=False):
    #Labels=True says, that the file contains labels
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    if labels:
        ctr_s = 0
        ctr_b = 0
        for row in csvreader:
            if int(row[0]) == 0:
                 ctr_b += 1
            elif int(row[0]) == 1:
                 ctr_s += 1
            else:
                 print(row)
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
        file.close()
        return np.array(rows), header, ctr_s, ctr_b
    else:
        for row in csvreader:
            x = np.array(row)
            x = x.astype(np.float)
            rows.append(x)
    file.close()
    return np.array(rows), header, len(rows)

def cut_based_sel(sig):
    sqrtsMeV = 13.e6
    fin = root.TFile(sig,"read")
    tin = fin.Get("tree")
    n_entries = tin.GetEntries()
    ev_num = 0
    for i in range(n_entries):
        tin.GetEntry(i)
        match_n = 0 #0,1,2
        match_a = False
        match_c = False
        DiphotonP4 = tin.PhotonP4[0] + tin.PhotonP4[1]
        if (DiphotonP4.M() < 150*1000 or DiphotonP4.M() > 1600*1000):
            continue
        phxia = DiphotonP4.M() / sqrtsMeV * math.exp(DiphotonP4.Rapidity())
        phxic = DiphotonP4.M() / sqrtsMeV * math.exp(-DiphotonP4.Rapidity())
        Dphi = root.TVector2.Phi_mpi_pi(tin.PhotonP4[0].Phi() - tin.PhotonP4[1].Phi())
        acop = 1 - abs(Dphi) / math.pi
        ProtonN, prxia, prxic, DeltaXiA, DeltaXiC = Base.slimProton(phxia, phxic, tin.ProtonN, tin.ProtonXi, tin.ProtonAC)
        #acop, prxia, prxic, phxia, phxic
        if not (acop > 0 and acop < 0.01):
            continue 
        if (prxia > 0.035 and prxia < 0.08):
            match_a = True
        if (prxic > 0.035 and prxic < 0.08):
            match_c = True
        if match_c:
            if (abs(prxic - phxic) < (0.004 + 0.1*phxic)):
                match_n+=1
        if match_a:
            if (abs(prxia - phxia) < (0.004 + 0.1*phxia)):
                match_n+=1
        if match_n > 0:
            ev_num+=1
    return ev_num

def get_data_labels(sigs):
    labels = []
    data = []
    for idx, sig in enumerate(sigs):
        # define signal and background NTuples
        num = len(sigs) - idx
        fins = root.TFile(sig, "read")
        tins = fins.Get("tree")
        n_entries = tins.GetEntries()
        #print(n_entries,tinb.GetEntries())
        # signal data
        for i in range(n_entries):
            tins.GetEntry(i)
            Dphi = root.TVector2.Phi_mpi_pi(
            tins.PhotonP4[0].Phi() - tins.PhotonP4[1].Phi())
            Acoplanarity = 1 - abs(Dphi)/math.pi
            data.append([tins.PhotonN, Acoplanarity, tins.PhotonP4[0].Eta(
            ), tins.PhotonP4[1].Eta(), tins.PhotonP4[0].Pt(), tins.PhotonP4[1].Pt()])
            labels.append([1])
    data = np.array(data)
    header = ['PhotonN', 'Acoplanarity', 'Eta0', 'Eta1', 'Pt0', 'Pt1']
    filename = 'tmp_data.csv'
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(data)

    filename = 'tmp_labels.csv'
    header = ['labels']
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerows(labels)
    return n_entries

def save_table(header, values_og, values_sel, values_net): 
    filename = 'events_through_sel.csv'
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)
        csvwriter.writerow(values_og)
        csvwriter.writerow(values_sel)
        csvwriter.writerow(values_net)

def plot_graph(x_ax,y_ax,name):  
    x_ax = np.array(x_ax).astype(int)
    y_ax = np.array(y_ax)
    indices = np.argsort(x_ax)
    x_ax = x_ax[indices]
    y_ax = y_ax[indices]
    plt.cla()
    plt.clf()
    plt.plot(x_ax, y_ax,'g-',label='Linear Interpolation')
    plt.scatter(x_ax, y_ax,label='Data Points')
    cs = CubicSpline(x_ax,y_ax)
    c_x = np.arange(200,1600)
    c_y = cs(c_x)
    plt.plot(c_x,c_y,'r--',label='Cubic Interpolation')
    plt.xlabel("${m_x}$ [GeV]")
    plt.ylabel("Efficiency")
    plt.legend()
    plt.savefig(name,bbox_inches='tight')

def transform(data):
    mean = [ 2.00031765, 5.41795687e-02, -6.95710764e-03, 1.66937028e-03, 2.75655885e+05, 2.60397571e+05] 
    std =[1.78199514e-02, 1.35194891e-01, 1.11637435e+00, 1.13354112e+00, 1.92910203e+05, 1.92324729e+05]
    return (data-mean)/std

if __name__ == '__main__':
    BATCH_SIZE = 64
    NUM_WORKERS = 10 
    device = get_device()
    model = load_model(device)
    x_ax = []
    y_ax = []
    y_cut = []
    y_og = []
    thresh = 0.4# 0.134 
    signal = "data.root"
    cut_num = cut_based_sel(signal)
    data, _, n_entries = csv2arr('recorded_data.csv')
    data = transform(data)
    val_dataset = Dataset_tr(torch.from_numpy(data))
    val_dataloader = DataLoader(
    dataset=val_dataset, batch_size=1, shuffle=True)
    print(cut_num)    
    ev_num  = signal_passed(
    model, val_loader=val_dataloader, device=device, thr = thresh)
    print("ev_num: ",ev_num)
    y_ax.append(ev_num) #For EL events 10000 for SD events 30000
    y_cut.append(cut_num)
    y_og.append(n_entries)
    save_table("Recorded data", y_og, y_cut, y_ax)
